import React, { useState } from "react";
import { todos as todosList } from "./todos.js";
import { v4 as uuid } from "uuid";
import TodoList from "./TodoList/TodoList";
import { Route, Switch } from "react-router";
//followed along with the todo pt 1 video for assignment

function App() {
  const [Todos, setTodos] = useState(todosList);
  const [InputText, setInputText] = useState("");
  const handleAddTodo = (event) => {
    //found online that using nativeEvent.code instead of .which gives a more readable result
    let KeyPressed = event.nativeEvent.code;
    if (KeyPressed === "Enter") {
      const newId = uuid();
      const newTodo = {
        userId: 1,
        id: newId,
        title: InputText,
        completed: false,
      };
      const newTodos = {
        ...Todos,
      };
      newTodos[newId] = newTodo;
      setTodos(newTodos);
      setInputText("");
    }
  };
  const handleToggle = (id) => {
    const newTodos = { ...Todos };
    newTodos[id].completed = !newTodos[id].completed;
    setTodos(newTodos);
  };
  const handleDeleteTodo = (id) => {
    const newTodos = { ...Todos };
    delete newTodos[id];
    setTodos(newTodos);
  };
  const handleClearCompleted = () => {
    const newTodos = { ...Todos };
    for (const Todo in newTodos) {
      if (newTodos[Todo].completed) {
        delete newTodos[Todo];
      }
    }
    setTodos(newTodos);
  };
  let activeTodos = Object.values(Todos).filter(
    (todo) => todo.completed === false
  );
  return (
    <section className="todoapp">
      <header className="header">
        <h1>Todos</h1>
        <input
          onChange={(event) => setInputText(event.target.value)}
          onKeyDown={(event) => handleAddTodo(event)}
          className="new-todo"
          value={InputText}
          placeholder="What needs to be done?"
          autoFocus
        />
      </header>
      <Switch>
        <Route exact path="/">
          <TodoList
            Todos={Object.values(Todos)}
            handleToggle={handleToggle}
            handleDeleteTodo={handleDeleteTodo}
          />
        </Route>
        <Route exact path="/active">
          <TodoList
            //Jon and Nico helped me with implementing the filter
            Todos={activeTodos}
            handleToggle={handleToggle}
            handleDeleteTodo={handleDeleteTodo}
          />
        </Route>
        <Route exact path="/completed">
          <TodoList
            Todos={Object.values(Todos).filter(
              (todo) => todo.completed === true
            )}
            handleToggle={handleToggle}
            handleDeleteTodo={handleDeleteTodo}
          />
        </Route>
      </Switch>
      <footer className="footer">
        {/* <!-- This should be `0 items left` by default --> */}
        <span className="todo-count">
          {/*searched online for a million years to find how to get object length*/}
          <strong>{Object.keys(activeTodos).length} </strong>
          item(s) left
        </span>
        <ul className="filters">
          <Switch>
            <Route exact path="/">
              <li>
                <a href="/" className="selected">
                  All
                </a>
              </li>
              <li>
                <a href="/active">Active</a>
              </li>
              <li>
                <a href="/completed">Completed</a>
              </li>
            </Route>
            <Route exact path="/active">
              <li>
                <a href="/">All</a>
              </li>
              <li>
                <a href="/active" className="selected">
                  Active
                </a>
              </li>
              <li>
                <a href="/completed">Completed</a>
              </li>
            </Route>
            <Route exact path="/completed">
              <li>
                <a href="/">All</a>
              </li>
              <li>
                <a href="/active">Active</a>
              </li>
              <li>
                <a href="/completed" className="selected">
                  Completed
                </a>
              </li>
            </Route>
          </Switch>
        </ul>
        <button
          className="clear-completed"
          onClick={() => handleClearCompleted()}
        >
          Clear completed
        </button>
      </footer>
    </section>
  );
}
export default App;
